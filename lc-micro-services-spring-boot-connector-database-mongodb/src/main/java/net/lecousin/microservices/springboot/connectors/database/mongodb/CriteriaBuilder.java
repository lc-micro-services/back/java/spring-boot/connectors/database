package net.lecousin.microservices.springboot.connectors.database.mongodb;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.query.Criteria;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.lecousin.microservices.commons.condition.Condition;
import net.lecousin.microservices.commons.condition.ConditionAnd;
import net.lecousin.microservices.commons.condition.ConditionOr;
import net.lecousin.microservices.commons.condition.ConditionVisitor;
import net.lecousin.microservices.commons.condition.FieldCondition;
import net.lecousin.microservices.commons.mapping.Mappers;
import net.lecousin.microservices.commons.reflection.ClassProperty;
import net.lecousin.microservices.commons.reflection.ReflectionUtils;
import net.lecousin.microservices.springboot.connectors.database.annotations.GeneratedValue;

@AllArgsConstructor
public class CriteriaBuilder implements ConditionVisitor {

	@Getter
	private Criteria criteria;
	private Class<?> type;
	
	@Override
	public Condition visit(ConditionAnd and) {
		List<Criteria> list = and.getAnd().stream()
			.map(c -> {
				CriteriaBuilder builder = new CriteriaBuilder(new Criteria(), type);
				c.accept(builder);
				return builder.getCriteria();
			})
			.toList();
		if (!list.isEmpty())
			criteria = criteria.andOperator(list);
		return and;
	}
	
	@Override
	public Condition visit(ConditionOr or) {
		List<Criteria> list = or.getOr().stream()
			.map(c -> {
				CriteriaBuilder builder = new CriteriaBuilder(new Criteria(), type);
				c.accept(builder);
				return builder.getCriteria();
			})
			.toList();
		if (!list.isEmpty())
			criteria = criteria.andOperator(list);
		return or;
	}
	
	@Override
	public Condition visit(FieldCondition field) {
		String fieldName = field.getField();
		Object value = field.getValue().getValue();
		
		var propertyOpt = ReflectionUtils.getClassProperty(type, fieldName);
		if (propertyOpt.isEmpty()) return field;
		ClassProperty property = propertyOpt.get();
		
		if (value != null)
			value = Mappers.map(value, property.getType());

		if (property.hasAnnotation(Id.class)) {
			fieldName = "_id";
			if (property.hasAnnotation(GeneratedValue.class)) {
				value = Mappers.map(value, ObjectId.class);
			}
		}
		
		criteria = Criteria.where(fieldName);
		switch (field.getValue().getOperator()) {
		case EQ: criteria = criteria.is(value); break;
		case NOT_EQ: criteria = criteria.ne(value); break;
		case LESS: criteria = criteria.lt(value); break;
		case LESS_OR_EQ: criteria = criteria.lte(value); break;
		case GREATER: criteria = criteria.gt(value); break;
		case GREATER_OR_EQ: criteria = criteria.gte(value); break;
		case IN: criteria = criteria.in(value); break;
		case NOT_IN: criteria = criteria.nin(value); break;
		}
		return field;
	}
	
}
