package net.lecousin.microservices.springboot.connectors.database.mongodb;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.mutable.MutableObject;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.mongodb.reactivestreams.client.FindPublisher;
import com.mongodb.reactivestreams.client.MongoClient;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.lecousin.microservices.commons.api.PageResponse;
import net.lecousin.microservices.commons.condition.Condition;
import net.lecousin.microservices.commons.mapping.Mappers;
import net.lecousin.microservices.springboot.connectors.database.DatabaseConnector;
import net.lecousin.microservices.springboot.connectors.database.EntityMeta;
import net.lecousin.microservices.springboot.connectors.database.annotations.GeneratedValue;
import net.lecousin.microservices.springboot.connectors.database.mongodb.mappers.MongoConverters;
import net.lecousin.microservices.springboot.connectors.database.request.FindRequest;
import net.lecousin.microservices.springboot.connectors.database.utils.PagingUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Slf4j
public class MongoDbConnector extends DatabaseConnector {

	private final MongoClient client;
	private final ReactiveMongoTemplate mongo;
	
	@Override
	protected <T> Mono<PageResponse<T>> doFind(FindRequest<T> request, EntityMeta meta) {
		Criteria criteria = new Criteria();
		if (request.getWhere().isPresent()) {
			CriteriaBuilder builder = new CriteriaBuilder(criteria, request.getEntityType());
			request.getWhere().get().accept(builder);
			criteria = builder.getCriteria();
		}
		
		String collectionName = meta.getName();
		Document filter = criteria.getCriteriaObject();
		log.debug("Search entities on collection {} with filter {}", collectionName, filter);
		
		return mongo.getCollection(collectionName)
			.flatMap(col -> {
				Mono<Optional<Long>> count;
				if (request.getPaging().isPresent() && request.getPaging().get().isWithTotal())
					count = Mono.from(col.countDocuments(filter)).map(Optional::of);
				else
					count = Mono.just(Optional.empty());

				MutableObject<FindPublisher<Document>> find = new MutableObject<>(col.find(filter));
				request.getPaging().map(PagingUtils::toPageable).filter(p -> p.isPaged()).ifPresent(paging -> {
					find.setValue(find.getValue().skip((int) paging.getOffset()).limit(paging.getPageSize()));
					if (paging.getSort() != null) {
						Document document = new Document();
						paging.getSort().forEach(order -> document.put(order.getProperty(), order.isAscending() ? 1 : -1));
						find.setValue(find.getValue().sort(document));
					}
				});

				Mono<List<T>> data = Flux.from(find.getValue())
					.map(doc -> {
						if (doc.containsKey("_id")) {
							meta.getIdProperty().ifPresent(p -> {
								doc.put(p.getName(), doc.get("_id"));
								doc.remove("_id");
							});
						}
						return Mappers.map(doc, request.getEntityType());
					})
					.collectList();
				return Mono.zip(data, count);
			})
			.map(tuple -> new PageResponse<>(tuple.getT2().orElse(null), tuple.getT1()));
	}
	
	@Override
	protected <T> Mono<T> doCreate(T entity, EntityMeta meta) {
		Document doc = new Document();
		for (var prop : meta.getProperties().values()) {
			if (prop.hasAnnotation(Id.class) && prop.hasAnnotation(GeneratedValue.class)) {
				ObjectId id = new ObjectId();
				doc.append("_id", id);
				prop.setValue(entity, Mappers.map(id, prop.getType()));
			} else if (prop.hasAnnotation(Version.class)) {
				doc.append(prop.getName(), 1L);
				prop.setValue(entity, 1L);
			} else {
				doc.append(prop.getName(), MongoConverters.toMongo(prop.getValue(entity)));
			}
		}
		log.debug("Create entity on collection {}: {}", meta.getName(), doc);
		return mongo.getCollection(meta.getName())
			.flatMap(col -> Mono.from(col.insertOne(doc)))
			.map(r -> entity);
	}
	
	@Override
	protected <T> Mono<T> doUpdate(T entity, EntityMeta meta) {
		return Mono.error(new Exception("TODO")); // TODO
	}

	@Override
	protected <T> Mono<Void> doDelete(Class<T> entityType, EntityMeta meta, Condition where) {
		Query q = new Query();
		if (where != null) {
			CriteriaBuilder builder = new CriteriaBuilder(new Criteria(), entityType);
			where.accept(builder);
			q.addCriteria(builder.getCriteria());
		}
		return mongo.remove(q, meta.getName()).then();
	}
	
	@Override
	protected Mono<Void> autoCreate(EntityMeta meta) {
		return Mono.empty(); // TODO
	}
	
	@Override
	public Mono<Void> destroy() {
		log.info("Closing MongDB Database Connector");
		return Mono.fromRunnable(client::close);
	}
}
