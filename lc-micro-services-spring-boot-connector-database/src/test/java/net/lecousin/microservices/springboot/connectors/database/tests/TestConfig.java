package net.lecousin.microservices.springboot.connectors.database.tests;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import net.lecousin.microservices.springboot.connectors.database.DatabaseConnectorConfiguration;

@Configuration
@EnableAutoConfiguration
@Import(DatabaseConnectorConfiguration.class)
public class TestConfig {

}
