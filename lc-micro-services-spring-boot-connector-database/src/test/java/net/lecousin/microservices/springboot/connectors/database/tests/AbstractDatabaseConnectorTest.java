package net.lecousin.microservices.springboot.connectors.database.tests;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;

import net.lecousin.microservices.commons.api.PageRequest;
import net.lecousin.microservices.springboot.commons.connector.ConnectorService;
import net.lecousin.microservices.springboot.connectors.database.DatabaseConnector;

@SpringBootTest(classes = TestConfig.class, webEnvironment = WebEnvironment.NONE)
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public abstract class AbstractDatabaseConnectorTest {

	@Autowired private ConnectorService connectorService;
	
	protected DatabaseConnector db;
	
	protected abstract String getImplementationName();
	
	protected abstract Map<String, Object> getProperties();
	
	@BeforeEach
	void setUp() {
		db = connectorService.getConnector(DatabaseConnector.class, getImplementationName(), "test", getProperties()).block();
	}
	
	@Test
	@Order(1)
	void testSimpleEntity() {
		var entity = db.create(SimpleEntity.builder().integer(1234).build()).block();
		assertThat(entity).isNotNull();
		assertThat(entity.getInteger()).isEqualTo(1234);
		assertThat(entity.getId()).isNotBlank();
		
		var page = db.find(SimpleEntity.class).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).hasSize(1).containsExactly(entity);

		var entity2 = db.create(SimpleEntity.builder().integer(4321).build()).block();
		assertThat(entity2).isNotNull();
		assertThat(entity2.getInteger()).isEqualTo(4321);
		assertThat(entity2.getId()).isNotBlank();

		page = db.find(SimpleEntity.class).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).hasSize(2).containsExactlyInAnyOrder(entity, entity2);
		assertThat(page.getTotal()).isNull();
		
		page = db.find(SimpleEntity.class).paging(PageRequest.builder().page(0).pageSize(10).withTotal(true).build()).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).hasSize(2).containsExactlyInAnyOrder(entity, entity2);
		assertThat(page.getTotal()).isEqualTo(2L);
		
		var e = db.findById(SimpleEntity.class, entity.getId()).block();
		assertThat(e).isEqualTo(entity);
		
		e = db.findById(SimpleEntity.class, entity2.getId()).block();
		assertThat(e).isEqualTo(entity2);
		
		db.deleteById(SimpleEntity.class, entity.getId()).block();
		
		page = db.find(SimpleEntity.class).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).hasSize(1).containsExactly(entity2);
		
		db.deleteById(SimpleEntity.class, entity2.getId()).block();
		
		page = db.find(SimpleEntity.class).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).isEmpty();
	}
	
	@Test
	@Order(10)
	void testEntityWithNumbers() {
		var entity = EntityWithNumbers.builder()
			.b((byte) 1)
			.bo(Byte.valueOf((byte) 2))
			.s((short) 3)
			.so(Short.valueOf((short) 4))
			.i(5)
			.io(6)
			.l(7L)
			.lo(8L)
			.f(9.12f)
			.fo(Float.valueOf(10.23f))
			.d(11.45d)
			.dob(Double.valueOf(12.67d))
			.build();
		entity = db.create(entity).block();
		assertThat(entity).isNotNull();
		assertThat(entity.getId()).isNotBlank();
		assertThat(entity.getB()).isEqualTo((byte) 1);
		assertThat(entity.getBo()).isEqualTo((byte) 2);
		assertThat(entity.getS()).isEqualTo((short) 3);
		assertThat(entity.getSo()).isEqualTo((short) 4);
		assertThat(entity.getI()).isEqualTo(5);
		assertThat(entity.getIo()).isEqualTo(6);
		assertThat(entity.getL()).isEqualTo(7L);
		assertThat(entity.getLo()).isEqualTo(8L);
		assertThat(entity.getF()).isEqualTo(9.12f);
		assertThat(entity.getFo()).isEqualTo(10.23f);
		assertThat(entity.getD()).isEqualTo(11.45d);
		assertThat(entity.getDob()).isEqualTo(12.67d);
		
		var page = db.find(EntityWithNumbers.class).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).hasSize(1).containsExactly(entity);
		
		var entity2 = EntityWithNumbers.builder()
			.b((byte) 1)
			.s((short) 3)
			.i(5)
			.l(7L)
			.f(9.12f)
			.d(11.45d)
			.build();
		entity2 = db.create(entity2).block();
		assertThat(entity2).isNotNull();
		assertThat(entity2.getId()).isNotBlank();
		assertThat(entity2.getB()).isEqualTo((byte) 1);
		assertThat(entity2.getBo()).isNull();
		assertThat(entity2.getS()).isEqualTo((short) 3);
		assertThat(entity2.getSo()).isNull();
		assertThat(entity2.getI()).isEqualTo(5);
		assertThat(entity2.getIo()).isNull();
		assertThat(entity2.getL()).isEqualTo(7L);
		assertThat(entity2.getLo()).isNull();
		assertThat(entity2.getF()).isEqualTo(9.12f);
		assertThat(entity2.getFo()).isNull();
		assertThat(entity2.getD()).isEqualTo(11.45d);
		assertThat(entity2.getDob()).isNull();
		
		page = db.find(EntityWithNumbers.class).paging(PageRequest.builder().page(0).pageSize(10).withTotal(true).build()).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).hasSize(2).containsExactlyInAnyOrder(entity, entity2);
		assertThat(page.getTotal()).isEqualTo(2L);
		
		var e = db.findById(EntityWithNumbers.class, entity.getId()).block();
		assertThat(e).isEqualTo(entity);
		
		e = db.findById(EntityWithNumbers.class, entity2.getId()).block();
		assertThat(e).isEqualTo(entity2);
		
		db.deleteById(EntityWithNumbers.class, entity.getId()).block();
		
		page = db.find(EntityWithNumbers.class).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).hasSize(1).containsExactly(entity2);
		
		db.deleteById(EntityWithNumbers.class, entity2.getId()).block();
		
		page = db.find(EntityWithNumbers.class).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).isEmpty();
	}
	
	@Test
	@Order(50)
	void testEntityWithTemporals() {
		long timestamp = System.currentTimeMillis();
		var entity = EntityWithTemporals.builder()
			.instant(Instant.ofEpochMilli(timestamp))
			.localDate(LocalDate.now())
			.localTime(LocalTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.systemDefault()))
			.localDateTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.systemDefault()))
			.build();
		var created = db.create(entity).block();
		assertThat(created).isNotNull();
		assertThat(created.getId()).isNotBlank();
		assertThat(created.getInstant()).isEqualTo(entity.getInstant());
		assertThat(created.getLocalDate()).isEqualTo(entity.getLocalDate());
		assertThat(created.getLocalTime()).isEqualTo(entity.getLocalTime());
		assertThat(created.getLocalDateTime()).isEqualTo(entity.getLocalDateTime());
		
		var page = db.find(EntityWithTemporals.class).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).hasSize(1).containsExactly(created);
		
		db.deleteById(EntityWithTemporals.class, entity.getId()).block();
		
		page = db.find(EntityWithTemporals.class).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).isEmpty();
	}

	
	@Test
	@Order(100)
	void testEntityWithOptional() {
		var entity = EntityWithOptional.builder()
			.str(Optional.of("test"))
			.build();
		var created = db.create(entity).block();
		assertThat(created).isNotNull();
		assertThat(created.getId()).isNotBlank();
		assertThat(created.getStr()).isPresent().contains("test");
		
		var page = db.find(EntityWithOptional.class).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).hasSize(1).containsExactly(created);
		
		var entity2 = EntityWithOptional.builder()
			.str(Optional.empty())
			.build();
		entity2 = db.create(entity2).block();
		assertThat(entity2).isNotNull();
		assertThat(entity2.getId()).isNotBlank();
		assertThat(entity2.getStr()).isEmpty();
		
		page = db.find(EntityWithOptional.class).paging(PageRequest.builder().page(0).pageSize(10).withTotal(true).build()).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).hasSize(2).containsExactlyInAnyOrder(created, entity2);
		assertThat(page.getTotal()).isEqualTo(2L);
		
		var e = db.findById(EntityWithOptional.class, entity.getId()).block();
		assertThat(e).isEqualTo(created);
		
		e = db.findById(EntityWithOptional.class, entity2.getId()).block();
		assertThat(e).isEqualTo(entity2);
		
		db.deleteById(EntityWithOptional.class, entity.getId()).block();
		
		page = db.find(EntityWithOptional.class).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).hasSize(1).containsExactly(entity2);
		
		db.deleteById(EntityWithOptional.class, entity2.getId()).block();
		
		page = db.find(EntityWithOptional.class).execute().block();
		assertThat(page).isNotNull();
		assertThat(page.getData()).isEmpty();
	}
	
}
