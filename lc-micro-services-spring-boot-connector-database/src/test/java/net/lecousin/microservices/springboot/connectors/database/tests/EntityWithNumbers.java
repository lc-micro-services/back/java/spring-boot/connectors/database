package net.lecousin.microservices.springboot.connectors.database.tests;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.lecousin.microservices.springboot.connectors.database.annotations.Entity;
import net.lecousin.microservices.springboot.connectors.database.annotations.GeneratedValue;

@Entity(domain = "test", name = "numbers")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EntityWithNumbers {

	@Id @GeneratedValue
	private String id;
	
	private byte b;
	private Byte bo;
	private short s;
	private Short so;
	private int i;
	private Integer io;
	private long l;
	private Long lo;
	private float f;
	private Float fo;
	private double d;
	private Double dob;
	
}
