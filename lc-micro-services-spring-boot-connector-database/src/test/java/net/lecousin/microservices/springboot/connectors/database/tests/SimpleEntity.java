package net.lecousin.microservices.springboot.connectors.database.tests;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.lecousin.microservices.springboot.connectors.database.annotations.Entity;
import net.lecousin.microservices.springboot.connectors.database.annotations.GeneratedValue;

@Entity(domain = "test", name = "simple")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SimpleEntity {

	@Id @GeneratedValue
	private String id;
	
	private int integer;
	
}
