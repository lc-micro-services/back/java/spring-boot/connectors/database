package net.lecousin.microservices.springboot.connectors.database.request;

import java.util.Optional;
import java.util.function.Function;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.lecousin.microservices.commons.api.PageRequest;
import net.lecousin.microservices.commons.api.PageResponse;
import net.lecousin.microservices.commons.condition.Condition;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class FindRequest<T> {

	@Getter
	private final Class<T> entityType;
	private final Function<FindRequest<T>, Mono<PageResponse<T>>> executor;

	@Getter
	private Optional<Condition> where = Optional.empty();
	@Getter
	private Optional<PageRequest> paging = Optional.empty();
	
	public FindRequest<T> where(Optional<Condition> condition) {
		this.where = condition;
		return this;
	}

	public FindRequest<T> where(Condition condition) {
		return where(Optional.ofNullable(condition));
	}
	
	public FindRequest<T> paging(Optional<PageRequest> pageRequest) {
		this.paging = pageRequest;
		return this;
	}

	public FindRequest<T> paging(PageRequest pageRequest) {
		return paging(Optional.ofNullable(pageRequest));
	}
	
	public Mono<PageResponse<T>> execute() {
		return executor.apply(this);
	}
	
}
