package net.lecousin.microservices.springboot.connectors.database;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.SimpleMetadataReaderFactory;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.lecousin.microservices.commons.api.PageRequest;
import net.lecousin.microservices.commons.api.PageResponse;
import net.lecousin.microservices.commons.condition.Condition;
import net.lecousin.microservices.commons.condition.ValueCondition.Operator;
import net.lecousin.microservices.commons.mapping.Mappers;
import net.lecousin.microservices.springboot.commons.connector.Connector;
import net.lecousin.microservices.springboot.connectors.database.annotations.Entity;
import net.lecousin.microservices.springboot.connectors.database.request.FindRequest;
import reactor.core.publisher.Mono;

@Slf4j
public abstract class DatabaseConnector implements Connector, ApplicationContextAware, InitializingBean {

	protected static final Map<Class<?>, EntityMeta> META = new HashMap<>();
	@Setter
	protected ApplicationContext applicationContext;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		synchronized (META) {
			if (META.isEmpty()) loadMeta();
		}
	}
	
	private void loadMeta() throws IOException {
		long startTime = System.currentTimeMillis();
		PathMatchingResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
		Resource[] classResources = resourceResolver.getResources(ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + "**/*.class");
		SimpleMetadataReaderFactory metadataReaderFactory = new SimpleMetadataReaderFactory();
		String annotationName = Entity.class.getName();
		for (Resource classResource : classResources) {
			try {
				MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(classResource);
				AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
				if (annotationMetadata.hasAnnotation(annotationName)) {
					Class<?> clazz = Class.forName(annotationMetadata.getClassName());
					try {
						META.put(clazz, new EntityMeta(clazz, applicationContext));
					} catch (Exception e) {
						log.error("Error analyzing entity {}", clazz.getName(), e);
					}
				}
			} catch (@SuppressWarnings("java:S1181") Throwable t) {
				// ignore
			}
		}
		log.info("{} entity classes found in {} ms.", META.size(), System.currentTimeMillis() - startTime);
	}
	
	protected abstract Mono<Void> autoCreate(EntityMeta meta);
	
	private Map<Class<?>, Mono<Void>> autoCreateDone = new HashMap<>();
	
	private Mono<EntityMeta> checkAutoCreate(Class<?> entityType) {
		return Mono.defer(() -> {
			EntityMeta meta = META.get(entityType);
			if (meta == null) return Mono.error(new IllegalArgumentException("Unknown entity " + entityType.getName()));
			Mono<Void> mono;
			synchronized (autoCreateDone) {
				mono = autoCreateDone.computeIfAbsent(entityType, k -> autoCreate(meta).cache());
			}
			return mono.thenReturn(meta);
		});
		
	}

	public final <T> FindRequest<T> find(Class<T> entityType) {
		return new FindRequest<>(entityType, this::executeFind);
	}
	
	public final <T> Mono<T> findById(Class<T> entityType, Serializable id) {
		Objects.requireNonNull(entityType, "entityType");
		Objects.requireNonNull(id, "id");
		return Mono.fromCallable(() -> META.get(entityType))
			.switchIfEmpty(Mono.defer(() -> Mono.error(new IllegalStateException("Unknown entity " + entityType.getName()))))
			.flatMap(meta -> Mono.justOrEmpty(meta.getIdName()))
			.switchIfEmpty(Mono.defer(() -> Mono.error(new IllegalStateException("No @Id found on entity " + entityType.getName()))))
			.flatMap(idName -> 
				find(entityType)
				.where(Condition.field(idName, Operator.EQ, id))
				.paging(PageRequest.builder().page(0).pageSize(1).withTotal(false).build())
				.execute()
			).flatMap(page -> Mono.justOrEmpty(page.first()));
	}
	
	protected final <T> Mono<PageResponse<T>> executeFind(FindRequest<T> request) {
		return checkAutoCreate(request.getEntityType()).flatMap(meta -> doFind(request, meta));
	}

	protected abstract <T> Mono<PageResponse<T>> doFind(FindRequest<T> request, EntityMeta meta);
	
	public final <T> Mono<T> create(T entity) {
		Objects.requireNonNull(entity, "entity");
		return checkAutoCreate(entity.getClass()).flatMap(meta -> doCreate(entity, meta));
	}

	protected abstract <T> Mono<T> doCreate(T entity, EntityMeta meta);
	
	public final <I, T> Mono<T> update(I input, Class<T> entityType) {
		Objects.requireNonNull(input, "input");
		Objects.requireNonNull(entityType, "entityType");
		return update(Mappers.map(input, entityType));
	}
	
	public final <T> Mono<T> update(T entity) {
		Objects.requireNonNull(entity, "entity");
		return checkAutoCreate(entity.getClass()).flatMap(meta -> doUpdate(entity, meta));
	}

	protected abstract <T> Mono<T> doUpdate(T entity, EntityMeta meta);
	
	public final <T> Mono<Void> delete(Class<T> entityType, Condition where) {
		Objects.requireNonNull(entityType, "entityType");
		return checkAutoCreate(entityType).flatMap(meta -> doDelete(entityType, meta, where));
	}

	protected abstract <T> Mono<Void> doDelete(Class<T> entityType, EntityMeta meta, Condition where);
	
	public final <T> Mono<Void> deleteById(Class<T> entityType, Serializable id) {
		Objects.requireNonNull(entityType, "entityType");
		Objects.requireNonNull(id, "id");
		return checkAutoCreate(entityType).flatMap(meta -> doDelete(entityType, meta, Condition.field(meta.getIdName().get(), Operator.EQ, id)));
	}

	
}
