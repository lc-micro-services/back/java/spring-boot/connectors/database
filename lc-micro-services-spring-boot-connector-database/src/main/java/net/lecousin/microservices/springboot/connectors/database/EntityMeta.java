package net.lecousin.microservices.springboot.connectors.database;

import java.util.Map;
import java.util.Optional;

import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Profiles;
import org.springframework.data.annotation.Id;

import lombok.Getter;
import net.lecousin.microservices.commons.reflection.ClassProperty;
import net.lecousin.microservices.commons.reflection.ReflectionUtils;
import net.lecousin.microservices.springboot.connectors.database.annotations.Entity;

public class EntityMeta {

	@Getter
	private String name;
	@Getter
	private Map<String, ClassProperty> properties;
	@Getter
	private boolean autoCreate;
	
	public EntityMeta(Class<?> clazz, ApplicationContext ctx) {
		Entity e = clazz.getAnnotation(Entity.class);
		name = e.domain();
		if (!e.name().isBlank()) name = name + "_" + e.name();
		properties = ReflectionUtils.getAllProperties(clazz);
		autoCreate = ctx.getEnvironment().acceptsProfiles(Profiles.of(e.autoCreateForProfiles()));
	}
	
	public Optional<ClassProperty> getIdProperty() {
		return properties.values().stream().filter(p -> p.hasAnnotation(Id.class)).findAny();
	}
	
	public Optional<String> getIdName() {
		return getIdProperty().map(ClassProperty::getName);
	}
	
}
