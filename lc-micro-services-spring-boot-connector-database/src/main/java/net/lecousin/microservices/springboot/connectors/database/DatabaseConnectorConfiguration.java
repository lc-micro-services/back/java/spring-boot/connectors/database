package net.lecousin.microservices.springboot.connectors.database;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import net.lecousin.microservices.springboot.commons.connector.CommonsConnectorConfiguration;

@Configuration
@Import(CommonsConnectorConfiguration.class)
@ComponentScan
public class DatabaseConnectorConfiguration {

}
