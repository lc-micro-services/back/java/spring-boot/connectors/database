package net.lecousin.microservices.springboot.connectors.database.postgresql;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostgreSQLProperties {

	private String uri;
	
}
