package net.lecousin.microservices.springboot.connectors.database.postgresql;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.annotation.Version;
import org.springframework.data.r2dbc.dialect.DialectResolver;
import org.springframework.data.r2dbc.dialect.R2dbcDialect;
import org.springframework.data.relational.core.dialect.RenderContextFactory;
import org.springframework.data.relational.core.sql.AsteriskFromTable;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Delete;
import org.springframework.data.relational.core.sql.DeleteBuilder.BuildDelete;
import org.springframework.data.relational.core.sql.DeleteBuilder.DeleteWhere;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Insert;
import org.springframework.data.relational.core.sql.OrderByField;
import org.springframework.data.relational.core.sql.SQL;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.BuildSelect;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoin;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectOrdered;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectWhere;
import org.springframework.data.relational.core.sql.SimpleFunction;
import org.springframework.data.relational.core.sql.SqlIdentifier;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.data.relational.core.sql.render.RenderContext;
import org.springframework.r2dbc.core.DatabaseClient;

import lombok.RequiredArgsConstructor;
import net.lecousin.microservices.commons.api.PageResponse;
import net.lecousin.microservices.commons.condition.Condition;
import net.lecousin.microservices.commons.mapping.Mappers;
import net.lecousin.microservices.commons.reflection.ClassProperty;
import net.lecousin.microservices.springboot.connectors.database.DatabaseConnector;
import net.lecousin.microservices.springboot.connectors.database.EntityMeta;
import net.lecousin.microservices.springboot.connectors.database.annotations.GeneratedValue;
import net.lecousin.microservices.springboot.connectors.database.request.FindRequest;
import net.lecousin.microservices.springboot.connectors.database.utils.PagingUtils;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class PostgreSQLConnector extends DatabaseConnector implements InitializingBean {

	private final DatabaseClient r2dbc;
	
	private R2dbcDialect dialect;
	private PostgresqlDialect extendedDialect;
	private RenderContext renderContext;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		dialect = DialectResolver.getDialect(r2dbc.getConnectionFactory());
		extendedDialect = new PostgresqlDialect(dialect.getIdentifierProcessing());
		renderContext = new RenderContextFactory(dialect).createRenderContext();
		super.afterPropertiesSet();
	}
	
	@Override
	protected <T> Mono<PageResponse<T>> doFind(FindRequest<T> request, EntityMeta meta) {
		boolean needsCount = request.getPaging().isPresent() && request.getPaging().get().isWithTotal();
		SelectQuery dataQuery = new SelectQuery(r2dbc, dialect, renderContext);
		SelectQuery countQuery = needsCount ? new SelectQuery(r2dbc, dialect, renderContext) : null;
		
		Table table = Table.create(meta.getName());
		BuildSelect dataBuilder = Select.builder().select(AsteriskFromTable.create(table)).from(table);
		BuildSelect countBuilder = needsCount ? Select.builder().select(SimpleFunction.create("COUNT", List.of(AsteriskFromTable.create(table)))).from(table) : null;
		var optPage = request.getPaging().map(PagingUtils::toPageable);
		List<OrderByField> sort = List.of();
		if (optPage.isPresent()) {
			var page = optPage.get();
			if (page.isPaged())
				dataBuilder = ((SelectFromAndJoin) dataBuilder).limitOffset(page.getPageSize(), page.getOffset());
			sort = page.getSort().stream().map(order -> OrderByField.from(Column.create(SqlIdentifier.quoted(order.getProperty()), table), order.getDirection())).toList();
		}
		
		CriteriaBuilder dataCriteria = new CriteriaBuilder(meta, dataQuery);
		CriteriaBuilder countCriteria = new CriteriaBuilder(meta, dataQuery);
		request.getWhere().ifPresent(c -> c.accept(dataCriteria));
		if (needsCount) request.getWhere().ifPresent(c -> c.accept(countCriteria));
		
		if (dataCriteria.getCondition() != null)
			dataBuilder = ((SelectWhere) dataBuilder).where(dataCriteria.getCondition());
		if (countBuilder != null && countCriteria.getCondition() != null)
			countBuilder = ((SelectWhere) countBuilder).where(countCriteria.getCondition());
		
		if (!sort.isEmpty())
			dataBuilder = ((SelectOrdered) dataBuilder).orderBy(sort);
		
		dataQuery.setQuery(dataBuilder.build());
		if (countBuilder != null) countQuery.setQuery(countBuilder.build());
		
		var data = dataQuery.execute().fetch().all().map(row -> Mappers.map(row, request.getEntityType())).collectList();
		
		Mono<Optional<Long>> count = needsCount ? countQuery.execute().fetch().one().map(row -> Optional.ofNullable((Long) row.get("count"))) : Mono.just(Optional.empty());
		
		return Mono.zip(data, count).map(tuple -> new PageResponse<>(tuple.getT2().orElse(null), tuple.getT1()));
	}

	@Override
	protected <T> Mono<T> doCreate(T entity, EntityMeta meta) {
		InsertQuery query = new InsertQuery(r2dbc, dialect, renderContext);
		Table table = Table.create(meta.getName());
		List<Column> columns = new LinkedList<>();
		List<Expression> values = new LinkedList<>();
		List<String> generated = new LinkedList<>();
		for (var prop : meta.getProperties().values()) {
			if (prop.hasAnnotation(GeneratedValue.class)) {
				generated.add(prop.getName());
				continue;
			}
			Column col = Column.create(SqlIdentifier.quoted(prop.getName()), table);
			Object value;
			if (prop.hasAnnotation(Version.class)) {
				value = 1L;
				prop.setValue(entity, 1L);
			} else {
				value = prop.getValue(entity);
			}
			if (value instanceof Optional o)
				value = o.orElse(null);
			Expression e;
			if (value == null)
				e = SQL.nullLiteral();
			else
				e = query.marker(value);
			columns.add(col);
			values.add(e);
		}
		query.setQuery(Insert.builder().into(table).columns(columns).values(values).build());
		return query.execute()
		.filter(s -> s.returnGeneratedValues())
		.map((row, m) -> {
			for (String name : generated) {
				Object value = row.get(name);
				ClassProperty p = meta.getProperties().get(name);
				p.setValue(entity, Mappers.map(value, p.getType()));
			}
			return entity;
		}).first();
	}

	@Override
	protected <T> Mono<T> doUpdate(T entity, EntityMeta meta) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected <T> Mono<Void> doDelete(Class<T> entityType, EntityMeta meta, Condition where) {
		DeleteQuery query = new DeleteQuery(r2dbc, dialect, renderContext);
		Table table = Table.create(meta.getName());
		BuildDelete builder = Delete.builder().from(table);
		if (where != null) {
			CriteriaBuilder criteria = new CriteriaBuilder(meta, query);
			where.accept(criteria);
			builder = ((DeleteWhere) builder).where(criteria.getCondition());
		}
		query.setQuery(builder.build());
		
		return query.execute().then();
	}

	
	@Override
	protected Mono<Void> autoCreate(EntityMeta meta) {
		String sql = extendedDialect.createTable(Table.create(meta.getName()), meta.getProperties().values());
		return r2dbc.sql(sql).then();
	}
	
	@Override
	public Mono<Void> destroy() {
		return Mono.fromRunnable(() -> {
			// nothing
		});
	}
	
}
