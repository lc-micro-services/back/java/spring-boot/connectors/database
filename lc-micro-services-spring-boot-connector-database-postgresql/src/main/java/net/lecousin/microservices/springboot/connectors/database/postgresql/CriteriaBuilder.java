package net.lecousin.microservices.springboot.connectors.database.postgresql;

import java.util.UUID;

import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Conditions;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.SQL;
import org.springframework.data.relational.core.sql.SqlIdentifier;
import org.springframework.data.relational.core.sql.Table;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.lecousin.microservices.commons.condition.Condition;
import net.lecousin.microservices.commons.condition.ConditionAnd;
import net.lecousin.microservices.commons.condition.ConditionOr;
import net.lecousin.microservices.commons.condition.ConditionVisitor;
import net.lecousin.microservices.commons.condition.FieldCondition;
import net.lecousin.microservices.commons.mapping.Mappers;
import net.lecousin.microservices.commons.reflection.ResolvedType;
import net.lecousin.microservices.springboot.connectors.database.EntityMeta;
import net.lecousin.microservices.springboot.connectors.database.annotations.GeneratedValue;

@RequiredArgsConstructor
public class CriteriaBuilder implements ConditionVisitor {
	
	@Getter
	private org.springframework.data.relational.core.sql.Condition condition = null;
	private final EntityMeta meta;
	private final SqlQuery<?> query;

	@Override
	public Condition visit(ConditionAnd and) {
		org.springframework.data.relational.core.sql.Condition springAnd = null;
		for (var c : and.getAnd()) {
			CriteriaBuilder builder = new CriteriaBuilder(meta, query);
			c.accept(builder);
			var cd = builder.getCondition();
			if (cd != null) {
				if (springAnd == null) springAnd = builder.getCondition();
				else springAnd = springAnd.and(builder.getCondition());
			}
		}
		if (springAnd != null) {
			if (condition == null) condition = springAnd;
			else condition = condition.and(springAnd);
		}
		return and;
	}
	
	@Override
	public Condition visit(ConditionOr or) {
		org.springframework.data.relational.core.sql.Condition springOr = null;
		for (var c : or.getOr()) {
			CriteriaBuilder builder = new CriteriaBuilder(meta, query);
			c.accept(builder);
			var cd = builder.getCondition();
			if (cd != null) {
				if (springOr == null) springOr = builder.getCondition();
				else springOr = springOr.or(builder.getCondition());
			}
		}
		if (springOr != null) {
			if (condition == null) condition = springOr;
			else condition = condition.and(springOr);
		}
		return or;
	}
	
	@Override
	public Condition visit(FieldCondition field) {
		String fieldName = field.getField();
		Object value = field.getValue().getValue();
		
		var property = meta.getProperties().get(fieldName);
		if (property == null) return field;
		
		if (value != null)
			value = Mappers.map(value, property.getType());
		
		if (value != null && property.hasAnnotation(GeneratedValue.class) && property.getType() instanceof ResolvedType.SingleClass c && String.class.equals(c.getSingleClass()))
			value = UUID.fromString((String) value);
		
		Column columnExpr = Column.create(SqlIdentifier.quoted(fieldName), Table.create(meta.getName()));
		Expression valueExpr;
		if (value == null) valueExpr = SQL.nullLiteral();
		else valueExpr = query.marker(value);

		org.springframework.data.relational.core.sql.Condition springCondition = null;
		switch (field.getValue().getOperator()) {
		case EQ: springCondition = Conditions.isEqual(columnExpr, valueExpr); break;
		case NOT_EQ: springCondition = Conditions.isNotEqual(columnExpr, valueExpr); break;
		case LESS: springCondition = Conditions.isLess(columnExpr, valueExpr); break;
		case LESS_OR_EQ: springCondition = Conditions.isLessOrEqualTo(columnExpr, valueExpr); break;
		case GREATER: springCondition = Conditions.isGreater(columnExpr, valueExpr); break;
		case GREATER_OR_EQ: springCondition = Conditions.isGreaterOrEqualTo(columnExpr, valueExpr); break;
		case IN: springCondition = Conditions.in(columnExpr, valueExpr); break;
		case NOT_IN: springCondition = Conditions.notIn(columnExpr, valueExpr); break;
		}
		if (springCondition != null) {
			if (condition == null) condition = springCondition;
			else condition = condition.and(springCondition);
		}
		
		return field;
	}
	
}
