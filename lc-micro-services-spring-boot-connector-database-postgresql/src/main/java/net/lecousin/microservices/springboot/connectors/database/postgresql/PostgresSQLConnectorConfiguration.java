package net.lecousin.microservices.springboot.connectors.database.postgresql;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.r2dbc.R2dbcAutoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration(exclude = R2dbcAutoConfiguration.class)
public class PostgresSQLConnectorConfiguration {

}
